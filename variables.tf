variable "application" {
  type    = string
  default = null
}
variable "assume_role_policy" {
  type = string
}
variable "bucket_name" {
  type = string
}
variable "environment" {
  type = string
}
variable "handler" {
  type = string
}
variable "memory_size" {
  type = string
}
variable "path_prefix" {
  type = string
}
variable "policy" {
  type = string
}
variable "product" {
  type = string
}
variable "runtime" {
  type = string
}
variable "source_code_dir" {
  type = string
}
variable "timeout" {
  type = string
}
variable "use_case" {
  type = string
}
variable "publish" {
  type    = bool
  default = true
}
variable "environment_variables" {
  type    = map(any)
  default = {}
}
variable "attach_extra_policy" {
  type = bool
}
variable "architectures" {
  type    = list(any)
  default = ["arm64"]
}
variable "enable_tracing" {
  type    = bool
  default = false
}
variable "layers" {
  type    = list(any)
  default = []
}