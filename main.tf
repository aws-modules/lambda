data "archive_file" "this" {
  type        = "zip"
  source_dir  = var.source_code_dir
  output_path = "${var.path_prefix}/${local.global_name}.zip"
}

resource "aws_s3_bucket_object" "this" {
  bucket      = var.bucket_name
  key         = "${local.global_name}.zip"
  source      = data.archive_file.this.output_path
  source_hash = filemd5("${data.archive_file.this.output_path}")
  tags        = local.tags
}

resource "aws_iam_role_policy" "this" {
  count  = var.attach_extra_policy ? 1 : 0
  name   = local.global_name
  role   = aws_iam_role.this.id
  policy = var.policy
}

resource "aws_iam_role" "this" {
  name               = local.global_name
  assume_role_policy = var.assume_role_policy
  path               = "/"
  description        = local.global_name
  tags               = local.tags
}

resource "aws_iam_role_policy_attachment" "AWSLambdaBasicExecutionRole_Policy" {
  role       = aws_iam_role.this.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole"
}
resource "aws_iam_role_policy_attachment" "AWSXRayDaemonWriteAccess_Policy" {
  role       = aws_iam_role.this.name
  policy_arn = "arn:aws:iam::aws:policy/AWSXRayDaemonWriteAccess"
}

resource "aws_lambda_function" "this" {
  s3_bucket         = var.bucket_name
  architectures     = var.architectures
  s3_key            = aws_s3_bucket_object.this.key
  s3_object_version = aws_s3_bucket_object.this.version_id
  function_name     = local.global_name
  handler           = var.handler
  role              = aws_iam_role.this.arn
  description       = local.global_name
  memory_size       = var.memory_size
  runtime           = var.runtime
  timeout           = var.timeout
  publish           = var.publish
  dynamic "tracing_config" {
    for_each = var.enable_tracing ? tomap({ "1" = "1" }) : {}
    content {
      mode = "Active"
    }
  }
  dynamic "environment" {
    for_each = length(keys(var.environment_variables)) == 0 ? {} : tomap({ "environment_variables" = var.environment_variables })
    content {
      variables = environment.value
    }
  }
  source_code_hash = filebase64sha256(data.archive_file.this.output_path)
  tags             = local.tags
  layers           = var.layers
}
